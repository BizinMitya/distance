package com.example.dmitriy.distance;

/**
 * Created by Dmitriy on 15.09.2016.
 */
public class Point {
    private double latitude;
    private double longitude;
    private String status;

    public Point(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Point(double latitude, double longitude, String status) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getStatus() {
        return status;
    }
}
