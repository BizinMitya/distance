package com.example.dmitriy.distance;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private TextView distanceOnStraightTextView;
    private TextView yourAddressTextView;
    private TextView distanceOnRoadTextView;
    private EditText addressEditText;
    private Location location;
    private Button myAddressButton;
    private Button distanceToButton;
    private static final double EARTH_RADIUS = 6371d; // Радиус Земли

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        myAddressButton = (Button) findViewById(R.id.myAddressButton);
        distanceToButton = (Button) findViewById(R.id.distanceToButton);
        distanceOnStraightTextView = (TextView) findViewById(R.id.distanceOnStraightTextView);
        yourAddressTextView = (TextView) findViewById(R.id.yourAddressTextView);
        distanceOnRoadTextView = (TextView) findViewById(R.id.distanceOnRoadTextView);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        distanceToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location != null) {
                    String address = addressEditText.getText().toString();
                    AsyncTask asyncTask = new Geocoding(address, distanceOnStraightTextView, distanceOnRoadTextView, location);
                    asyncTask.execute();
                }else {
                    distanceOnStraightTextView.setText("Ваша локация не определена!");
                    distanceOnRoadTextView.setText("Ваша локация не определена!");
                }
            }
        });
        myAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location != null) {
                    AsyncTask asyncTask = new Geodecoding(yourAddressTextView, location);
                    asyncTask.execute();
                } else {
                    yourAddressTextView.setText("Ваша локация не определена!");
                }
            }
        });
    }

    private double distanceBetween2Points(Point point1, Point point2) {
        double dlng = toRadians(point1.getLongitude() - point2.getLongitude());
        double dlat = toRadians(point1.getLatitude() - point2.getLatitude());
        double a = sin(dlat / 2) * sin(dlat / 2) + cos(toRadians(point1.getLatitude())) * cos(toRadians(point2.getLatitude())) * sin(dlng / 2) * sin(dlng / 2);
        double c = 2 * atan2(sqrt(a), sqrt(1 - a));
        return c * EARTH_RADIUS;//в км
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            this.location = location;
        } else {
            distanceOnStraightTextView.setText("Ваша локация не определена!");
            distanceOnRoadTextView.setText("Ваша локация не определена!");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        distanceOnStraightTextView.setText("Включите gps");
        distanceOnRoadTextView.setText("Включите gps");
    }
}
