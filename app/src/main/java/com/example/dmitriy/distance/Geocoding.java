package com.example.dmitriy.distance;

import android.location.Location;
import android.os.AsyncTask;
import android.widget.TextView;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

/**
 * Created by Dmitriy on 15.09.2016.
 */
public class Geocoding extends AsyncTask {
    private static final double EARTH_RADIUS = 6371d; // Радиус Земли
    private String address;
    private TextView distanceOnStraightTextView;
    private TextView distanceOnRoadTextView;
    private Location location;
    private String distanceOnStraight;
    private String distanceOnRoad;

    public Geocoding(String address, TextView distanceOnStraightTextView, TextView distanceOnRoadTextView, Location location) {
        this.address = address;
        this.distanceOnStraightTextView = distanceOnStraightTextView;
        this.distanceOnRoadTextView = distanceOnRoadTextView;
        this.location = location;
    }

    @Override
    protected void onPostExecute(Object result) {
        distanceOnStraightTextView.setText(new StringBuffer(this.distanceOnStraight));
        distanceOnRoadTextView.setText(new StringBuffer(this.distanceOnRoad));
    }


    private double distanceBetween2Points(Point point1, Point point2) {
        double dlng = toRadians(point1.getLongitude() - point2.getLongitude());
        double dlat = toRadians(point1.getLatitude() - point2.getLatitude());
        double a = sin(dlat / 2) * sin(dlat / 2) + cos(toRadians(point1.getLatitude())) * cos(toRadians(point2.getLatitude())) * sin(dlng / 2) * sin(dlng / 2);
        double c = 2 * atan2(sqrt(a), sqrt(1 - a));
        return c * EARTH_RADIUS;//в км
    }

    private static String encodeParams(Map<String, String> params) {
        String paramsUrl = Joiner.on('&').join(// получаем значение вида key1=value1&key2=value2...
                Iterables.transform(params.entrySet(), new com.google.common.base.Function<Map.Entry<String, String>, Object>() {

                    @Override
                    public String apply(final Map.Entry<String, String> input) {
                        try {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append(input.getKey());// получаем значение вида key=value
                            stringBuffer.append('=');
                            stringBuffer.append(URLEncoder.encode(input.getValue(), "utf-8"));// кодируем строку в соответствии со стандартом HTML 4.01
                            return stringBuffer.toString();
                        } catch (final UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }));
        return paramsUrl;
    }

    private String distanceOnRoad(String address) throws IOException, JSONException {
        String distanceOnRoadUrl = "http://maps.googleapis.com/maps/api/distancematrix/json";
        Map<String, String> paramsForRoad = Maps.newHashMap();
        paramsForRoad.put("origins", location.getLatitude() + "," + location.getLongitude());
        paramsForRoad.put("destinations", address);
        String urlRoad = distanceOnRoadUrl + '?' + encodeParams(paramsForRoad);
        JSONObject responseRoad = JsonReader.read(urlRoad);
        String status = responseRoad.getString("status");
        JSONObject distance = responseRoad.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance");
        //JSONObject duration = row.getJSONArray("elements").getJSONObject(0);
        int value = distance.getInt("value");//в метрах
        return status.equals("OK") ? new String(((double) value / 1000d) + " км") : new String("");
    }

    private Point geoCoding(String address) throws IOException, JSONException {
        String url = "http://maps.googleapis.com/maps/api/geocode/json";// путь к Geocoding API по HTTP
        Map<String, String> params = Maps.newHashMap();
        params.put("sensor", "false");// исходит ли запрос на геокодирование от устройства с датчиком местоположения
        params.put("address", address);// адрес, который нужно геокодировать
        String urlParams = url + '?' + encodeParams(params);// генерируем путь с параметрами
        JSONObject jsonObject = JsonReader.read(urlParams);// делаем запрос к вебсервису и получаем от него ответ
        // как правило наиболее подходящий ответ первый и данные о координатах можно получить по пути
        // //results[0]/geometry/location/lng и //results[0]/geometry/location/lat
        JSONObject location = jsonObject.getJSONArray("results").getJSONObject(0);
        location = location.getJSONObject("geometry");
        location = location.getJSONObject("location");
        double lng = location.getDouble("lng");// долгота
        double lat = location.getDouble("lat");// широта
        String status = jsonObject.getString("status");
        return new Point(lat, lng, status);
    }

    @Override
    protected Void doInBackground(Object... params) {
        if (!address.equals("")) {
            if (location != null) {
                try {
                    Point point = geoCoding(address);
                    String distanceOnRoad = distanceOnRoad(address);
                    if (point.getStatus().equals("OK")) {
                        this.distanceOnStraight = String.format("%.3f", distanceBetween2Points(point, new Point(location.getLatitude(), location.getLongitude()))) + " км";
                    }
                    if (!distanceOnRoad.equals("")) {
                        System.out.println(distanceOnRoad);
                        this.distanceOnRoad = distanceOnRoad;
                    } else if (distanceOnRoad.equals("")) {
                        this.distanceOnRoad = "Возникла ошибка!";
                    }
                    if (point.getStatus().equals("ZERO_RESULTS")) {
                        this.distanceOnStraight = "Адрес не найден!";
                    }
                    if (point.getStatus().equals("OVER_QUERY_LIMIT")) {
                        this.distanceOnStraight = "Превышен лимит запросов!";
                    }
                    if (point.getStatus().equals("REQUEST_DENIED")) {
                        this.distanceOnStraight = "Запрос отклонён!";
                    }
                    if (point.getStatus().equals("INVALID_REQUEST")) {
                        this.distanceOnStraight = "Неправильный адрес!";
                    }
                    if (point.getStatus().equals("UNKNOWN_ERROR")) {
                        this.distanceOnStraight = "Ошибка!Повторите ещё раз!";
                    }
                } catch (IOException | JSONException e) {
                    this.distanceOnStraight = "Произошла ошибка!";
                    this.distanceOnRoad = "Произошла ошибка!";
                    e.printStackTrace();
                }
            } else {
                this.distanceOnStraight = "Ваша локация не определена!";
                this.distanceOnRoad = "Ваша локация не определена!";
            }
        } else {
            this.distanceOnStraight = "Пустой адрес!";
            this.distanceOnRoad = "Пустой адрес!";
        }
        return null;
    }

}
