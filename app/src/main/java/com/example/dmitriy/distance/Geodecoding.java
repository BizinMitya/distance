package com.example.dmitriy.distance;

import android.location.Location;
import android.os.AsyncTask;
import android.widget.TextView;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by Dmitriy on 16.09.2016.
 */
public class Geodecoding extends AsyncTask {
    private TextView textView;
    private Location location;
    private String result;

    public Geodecoding(TextView textView, Location location) {
        this.textView = textView;
        this.location = location;
    }

    @Override
    protected void onPostExecute(Object result) {
        textView.setText(new StringBuffer(this.result));
    }


    @Override
    protected Object doInBackground(Object[] params) {
        if (location != null) {
            try {
                result = geoDecoding(new Point(location.getLatitude(), location.getLongitude()));
            } catch (IOException | JSONException e) {
                result = "Произошла ошибка!";
            }
        } else {
            result = "Ваша локация не определена!";
        }
        return null;
    }

    private static String encodeParams(Map<String, String> params) {
        String paramsUrl = Joiner.on('&').join(// получаем значение вида key1=value1&key2=value2...
                Iterables.transform(params.entrySet(), new com.google.common.base.Function<Map.Entry<String, String>, Object>() {

                    @Override
                    public String apply(final Map.Entry<String, String> input) {
                        try {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append(input.getKey());// получаем значение вида key=value
                            stringBuffer.append('=');
                            stringBuffer.append(URLEncoder.encode(input.getValue(), "utf-8"));// кодируем строку в соответствии со стандартом HTML 4.01
                            return stringBuffer.toString();
                        } catch (final UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }));
        return paramsUrl;
    }

    private String geoDecoding(Point point) throws IOException, JSONException {
        String baseUrl = "http://maps.googleapis.com/maps/api/geocode/json";// путь к Geocoding API по HTTP
        Map<String, String> params = Maps.newHashMap();
        params.put("language", "ru");// язык данных, на котором мы хотим получить
        params.put("sensor", "false");// исходит ли запрос на геокодирование от устройства с датчиком местоположения
        // текстовое значение широты/долготы, для которого следует получить ближайший понятный человеку адрес, долгота и
        // широта разделяется запятой, берем из предыдущего примера
        params.put("latlng", point.getLatitude() + "," + point.getLongitude());
        String url = baseUrl + '?' + encodeParams(params);// генерируем путь с параметрами
        System.out.println(url);// Путь, что бы можно было посмотреть в браузере ответ службы
        JSONObject response = JsonReader.read(url);// делаем запрос к вебсервису и получаем от него ответ
        // как правило, наиболее подходящий ответ первый и данные об адресе можно получить по пути
        // //results[0]/formatted_address
        JSONObject location = response.getJSONArray("results").getJSONObject(0);
        String formattedAddress = location.getString("formatted_address");
        return formattedAddress;// итоговый адрес
    }
}
